module Main where

import System.Environment

import Commandline
import Transform

-- | It is that small, yes.
main :: IO ()
main = getArgs >>= parse >>= transform
