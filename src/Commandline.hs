{-# LANGUAGE TemplateHaskell #-}
module Commandline (parse, Task(..), Format(..), Mapping(..)) where

import Data.Char (toLower)
import Data.Monoid
import Options.Applicative

-- Automatic version replacement
import Data.Version (showVersion)
import Paths_filepic as Project (version)
filepicVersion = showVersion Project.version

-- | Configuration object for a transformation task.
data Task
    = Task
    { inFile :: String
    , outFile :: String
    , imageWidth :: Int
    , imageFormat :: Format
    , imageScale :: Int
    , colorMapping :: Mapping
    }

-- | All supported output formats
data Format = Bitmap | Jpeg | Png deriving Enum

-- | All supported color mappings
data Mapping = Greyscale | ColorGradient | ColorAbsolute deriving Enum

-- | Given a list of strings (the commandline arguments preferably), returns the
--   parsed 'Task' object. Prints an error message if the parsing fails.
parse :: [String] -> IO Task
parse = handleParseResult . execParserPure prefs parser
    where prefs = defaultPrefs

-- | Commandline parser. Handles help, version and normal call.
parser :: ParserInfo Task
parser = info (helper <*> version <*> (insertDefaultOutputFile <$> task))
       (  fullDesc
       <> header "filepic - Transform files into images"
       <> footer
            (  "Formats: bmp, jpg, png.\n"
            ++ "Mappings: greyscale, absolute, gradient\n"
            )
       )
    where
        task = Task <$> inputFile <*> outputFile <*> width <*> format <*> scale <*> mapping
        version = infoOption ("filepic v" ++ filepicVersion) $ mconcat
                [ short 'v'
                , long "version"
                , help "Show version information"
                , hidden
                ]

-- | Inserts the default argument for @outFile@ of the given 'Task', if needed.
insertDefaultOutputFile :: Task -> Task
insertDefaultOutputFile t
    | outFile t == "" = t { outFile = inFile t ++ ".bmp"}
    | otherwise       = t

-- | Parses the input file
inputFile :: Parser String
inputFile = argument str $ mconcat
    [ metavar "INPUT"
    , help "File to read from"
    ]

-- | Parses the output file
outputFile :: Parser String
outputFile = argument str $ mconcat
    [ metavar "OUTPUT"
    , help "File to write the bitmap to"
    , value ""
    , showDefaultWith (const "INPUT.bmp")
    ]

-- | Parses the width option
width :: Parser Int
width = option auto $ mconcat
    [ short 'w'
    , long "width"
    , value 600
    , help "Width of the bitmap"
    ]

-- | Parses the format option
format :: Parser Format
format = option (maybeReader formatReader) $ mconcat
    [ short 'f'
    , long "format"
    , value Bitmap
    , help "Type of image to produce"]
    where
        formatReader s = case map toLower s of
            "bitmap" -> Just Bitmap
            "jpg"    -> Just Jpeg
            "jpeg"   -> Just Jpeg
            "png"    -> Just Png
            _        -> Nothing

-- | Parses the scale option
scale :: Parser Int
scale = option auto $ mconcat
    [ short 's'
    , long "scale"
    , value 1
    , help "Scale of each pixel"]

-- | Parses the mapping option
mapping :: Parser Mapping
mapping = option (maybeReader mappingReader) $ mconcat
    [ short 'm'
    , long "mapping"
    , value Greyscale
    , help "Which mapping to apply"]
    where
        mappingReader s = case map toLower s of
            "greyscale"     -> Just Greyscale
            "gradient"      -> Just ColorGradient
            "absolute"      -> Just ColorAbsolute
            _               -> Nothing