module Transform (transform) where

import qualified Codec.Picture        as P
import qualified Data.ByteString      as B
import           Data.Fixed                (mod')
import qualified Data.Vector.Storable as V
import           Data.Word

import Commandline (Task(..), Format(..), Mapping(..))

-- | God action transforming files into bitmaps.
--   @transform (Task inFile outFile width)@ reads the data from @inFile@,
--   transforms it into a bitmap @width@ pixels wide and writes that bitmap to
--   @outFile@.
transform :: Task -> IO ()
transform t = do
    let width = imageWidth t
    let scale = imageScale t
    let mapping = case colorMapping t of
                    Greyscale     -> greyscale
                    ColorGradient -> gradientColor
                    ColorAbsolute -> absoluteColor
    content <- B.unpack <$> B.readFile (inFile t)
    let pixels = scaleTo scale width . stretchTo width nil $ mapping content
    let height = length pixels `div` width
    let image  = P.ImageRGB8 $ P.Image width height (V.fromList $ concatMap expand pixels)
    case imageFormat t of
        Bitmap -> P.saveBmpImage     (outFile t) image
        Jpeg   -> P.saveJpgImage 100 (outFile t) image
        Png    -> P.savePngImage     (outFile t) image

-- | @stretchTo n x xs@ makes sure @length xs@ evaluates to a multipe of @n@
--   by appending the smallest amount of @x@ to xs as are needed.
stretchTo :: Int -> a -> [a] -> [a]
stretchTo n x xs = xs ++ fill
    where
        fill = replicate (n - (length xs `rem` n)) x

-- | @scaleTo n w xs@ repeats each element of @xs@ @n@ times and repeats
--   every @w@ elements of @xs@ @n@ times.
--
--   >>> @scaleTo 2 5 [1,2,3,4,5]
--   [1,1,2,2,3,1,1,2,2,3,3,4,4,5,5,3,3,4,4,5,5]
scaleTo :: Int -> Int -> [a] -> [a]
scaleTo n w = concat . concatMap (replicate n) . chunksOf w . concatMap (replicate n)

-- | Splits a given list into chunks, where each chunk is the given amount
--   of elements wide (or less).
--
--   >>> chunksOf 2 [1..10]
--   [[1,2],[3,4],[5,6],[7,8],[9,10]]
chunksOf :: Int -> [a] -> [[a]]
chunksOf n xs | n <= 0    = error "Transform.chunksOf: Negative chunk length"
              | null xs   = []
              | otherwise = let (before, after) = splitAt n xs
                            in before : chunksOf n after

-- # Transformations

-- | Transforms a list of 'Word8's into a list of 'PixelRGB8's, where each
--   word denotes the brightness of a single pixel.
greyscale :: [Word8] -> [P.PixelRGB8]
greyscale = map (\x -> P.PixelRGB8 x x x)

-- | Transforms a list of 'Word8's into a list of 'PixelRGB8's, where each
--   word denotes the hue of a single pixel. Brightness and saturation are
--   both 100% for each pixel.
gradientColor :: [Word8] -> [P.PixelRGB8]
gradientColor = map (\x -> let (r, g, b) = hsvToRgb (x, 255, 255) in P.PixelRGB8 r g b)

-- | Transforms a list of 'Word8's into a list of 'PixelRGB8's, where every
--   *three* words correspond to a single pixel, the first pixel being the red
--   channel, the second one the green channel and the third one the blue
--   channel.
absoluteColor :: [Word8] -> [P.PixelRGB8]
absoluteColor = map toPixel . chunksOf 3
    where
        toPixel [r]       = P.PixelRGB8 r 0 0
        toPixel [r, g]    = P.PixelRGB8 r g 0
        toPixel [r, g, b] = P.PixelRGB8 r g b

-- # Utility

-- | An empty pixel. Black. nil. Darkness incarnate.
nil :: P.PixelRGB8
nil = P.PixelRGB8 0 0 0

-- | Converts a color given in HSV space into one given in RGB space.
--   As HSV usually has the hue in range [0..360] and the saturation and value
--   in range [0..1] or [0..100], this method uses atypical values due to being
--   restricted to 'Word8'.
--   All values should be seen as a kinda-percentage; Hue ranges from 0 (0°)
--   to 255 (360°) and is calculated using @hue / 255 * 360@.
--   Saturation and brightness are both calculated using @x / 255@.
--
--   Algorithm taken from <https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV>
hsvToRgb :: (Word8, Word8, Word8) -> (Word8, Word8, Word8)
hsvToRgb (h', s', v') = ( truncate $ (r' + m) * 255
                        , truncate $ (g' + m) * 255
                        , truncate $ (b' + m) * 255
                        )
    where
        h = (360 * fromInteger (toInteger h') / 255) :: Double
        s = (  1 * fromInteger (toInteger s') / 255) :: Double
        v = (  1 * fromInteger (toInteger v') / 255) :: Double
        chroma = v * s
        hue    = h / 60
        x      = chroma * (1 - abs ((hue `mod'` 2) - 1))
        m      = v - chroma
        (r', g', b') | hue `within` (0, 1) = (chroma, x, 0)
                     | hue `within` (1, 2) = (x, chroma, 0)
                     | hue `within` (2, 3) = (0, chroma, x)
                     | hue `within` (3, 4) = (0, x, chroma)
                     | hue `within` (4, 5) = (x, 0, chroma)
                     | hue `within` (5, 6) = (chroma, 0, x)

-- | Not-very-elegant utility to do "range"-comparisons (like x <= y <= z).
within :: Ord a => a -> (a, a) -> Bool
within y (x, z) = x <= y && y <= z

-- | Converts a pixel back to it's base component(s).
expand :: P.PixelRGB8 -> [Word8]
expand (P.PixelRGB8 r g b) = [r, g, b]