# filepic

`filepic` is a simple tool to generate an image from any file. It works by applying a mapping from single bytes to the RGB-color space and collecting those in a... well... image.

## Usage

`filepic` is built using [Haskell Stack](https://www.haskellstack.org/). Simply put it in your path and you're good to go.

### Building

Inside the project directory, run `stack build filepic`. Dependencies will be fetched, the code will be compiled and happiness dispensed in the form of ice cream. Alright, that last one was a lie.

### Installation

When built, stack places the executable somewhere whithing `.stack-work/`. To actually install it, run `stack build filepic --copy-bins` or `stack install filepic` for a shortcut. To find out where the executable will be put, run `stack path | grep "local-bin-path"`. You can even specify where to put the binaries with `stack install`, but for that you oughta RTFM.

### Running

`filepic` expects a single argument - the file to transform. Just run `filepic your-file.dunno` and a new file `your-file.dunno.bmp` will be created. If you supply a second argument, the bitmap will be written to that file. For more usage info, run `filepic --help`.

## Examples

The current executable file built on Windows looks like this:

![Executable as greyscale](examples/filepic.jpg)